![modderbrothers](../assets/product.png)

# Manuel - Colecovision - Cartouche 16 jeux

[English Version Here](./README.md)

## Outillage requit

Pour l'utilisation de la cartouche :
- Aucun

Pour la reprogrammation de l'EEPROM ou le changement d'EPROM/EEPROM :
- Tournevis
- Couteau sans dent
- Programmateur d'EPROM capable de programmer les series 27 (_**TL866 II Plus**_ ou autre)

## Utilisation de la cartouche

Insérez simplement la cartouche dans votre Colecovision, comme n'importe quelle autre cartouche de jeu, puis démarrez-la !
Le jeu correspondant à la position du petit bouton rotatif débutera.

Pour changer de jeu :
- Éteignez votre console
- Bougez le bouton rotatif sur la position souhaitée
- Rallumez votre console

Actionner le bouton rotatif lorsque la console est allumée n'endommagera ni votre console, ni la cartouche, mais aura pour effet de faire crasher le jeu en cours.

![modderbrothers](./assets/cartridge-in-action.png)

## Reprogrammation et/ou changement d'EPROM/EEPROM

#### Précautions générales

- Toutes les manipulations doivent être effectuées hors tension.
- Vous êtes responsables de votre outillage et de ses manipulations. Si vous n'avez pas les aptitudes et/ou l'expérience requise, nous vous conseillons de faire réaliser ces manipulations par une personne qualifiée.

#### Precautions à prendre avant de toucher les cartes et les composants électroniques

Certains composants sont très sensibles à l'électricité statique.
Veillez à vous décharger avant l'ouverture de votre cartouche console :
- Placez-vous dans une piece en carrelage, et évitez la moquette !
- Évitez les pulls en laine ou autres vêtements qui se chargent facilement !
- Touchez la carcasse métallique d'un appareil électrique pour vous décharger à la terre.

#### Ouverture de la cartouche

Dévissez simplement les vis de la cartouche jusqu'à séparation des 2 parties de la coque

![modderbrothers](./assets/cartridge-open.png)

#### Extraction de l'EPROM

Il faut maintenant extraire l'EEPROM de son support :
Pour ce faire, glissez ***très délicatement*** la lame du couteau entre l'EEPROM et son support.
Puis faites légèrement tourner le couteau dans le sens des aiguilles d'une montre et dans le sens inverse, pour que la lame soulève le chip d'un millimetre de chaque côté des pattes.
Répétez l'opération en alternance de chaque côté du support, jusqu'à ce que le chip sorte de lui-même.
Dans la mesure du possible, évitez de toucher les pattes métalliques.

_Faite attention à ne pas faire basculer la pointe du couteau sur la carte mère, cela pourrait endommager les pistes qui passent dessous !_

![modderbrothers](./assets/cartridge-eprom-extracted.png)

##### EPROM/EEPROM compatibles

Les EPROM compatibles sont toutes les EPROM 27C040 (512Ko soit 4MBits).
Les EEPROM compatibles sont toutes les EEPROM 27040 (W27E040, 27E040, ...)

Toutes les EPROM/EEPROM compatibles pin à pin avec la 27C040 doit pouvoir fonctionner.
Pour éviter tous soucis liés à la vitesse des chips, prenez les versions les plus rapides : celles ou le nombre derrière le '-' est le plus faible.

![modderbrothers](./assets/compatible-eprom-eeprom.png)

#### Reprogrammation

Pour changer vos jeux sur la cartouche, vous avez deux solutions :
- Effacer l'EPROM actuelle et la reprogrammer
- Programmer une nouvelle EPROM et remplacer celle d'origine (ce qui vous permet de la conserver et de pouvoir la remettre plus tard)

##### Effacer l'EPROM/EEPROM

Vous pouvez sauter cette étape si :
- Votre cartouche contient une EEPROM (Electrically Erasable Programmable Read-Only Memory), sans petite vitre apparente, ce qui est le cas sur les cartouches ModderBrothers par défaut.
- Vous avez opté pour un remplacement d'EPROM/EEPROM, vous pouvez sauter cette étape

Dans le dernier cas, il vous faudra vous assurer que votre EPROM est totalement effacée, grace a un petit appareil à Ultra-Violets qu'on peut trouver facilement, et pour pas cher ici :  
https://www.amazon.fr/s?k=eprom+uv+eraser
ou un peu moins cher ici :
https://fr.aliexpress.com/w/wholesale-eprom-uv-eraser.html

##### Construire votre fichier à flasher

Procurez-vous la dernière version de notre [Rom Merger](https://gitlab.com/modderbrothers/rom-merger-application/-/releases) (sous Windows uniquement pour l'instant)

Téléchargez l'application directement, puis décompressez-la dans le répertoire de votre choix, avant de la lancer.

![modderbrothers](./assets/rom-merger.png)

Son utilisation est extrêmement simple : commencez par sélectionner la bonne cartouche "Colecovision 16 games cartridge".

Ensuite, ajoutez vos fichiers roms au format ".bin" ou ".a26". Vous pouvez les réordonner comme vous le souhaitez, même après les avoir ajoutés. Lorsque le sélecteur de fichier s'ouvre, utilisez SHIFT ou CTRL pour sélectionner plusieurs fichiers d'un coup.
Les fichiers sélectionnés doivent, bien évidemment, respecter la limite des 32ko par jeu.

Une fois vos fichiers choisis et ordonnés, cliquez simplement sur "Sauver". L'application se chargera de construire un fichier à plat, _prêt à programmer_, contenant la totalité de vos roms.

Par défaut, la case à cocher "Autofill empty slot" est cochée. Cela signifie que si vous n'avez pas rempli les 16 slots disponibles, le logiciel comblera les manques en reprenant les premieres roms.

Une fois sauvé, l'application vous affiche un compte rendu des opérations.

![modderbrothers](./assets/rom-merger-saved.png)

À côté du fichier ".bin" qui aura été créé, vous trouverez également un fichier ".txt" du même nom, qui contiendra la liste des roms pour chaque slot. Exemple :

```
# 0 - Dukes of Hazzard, The (USA)
# 1 - Spy Hunter (USA)
# 2 - Super DK! (USA) (Proto)
# 3 - Buck Rogers - Planet of Zoom (USA, Europe)
# 4 - Defender (USA) (BUG Fixed)
# 5 - Donkey Kong (USA)
# 6 - Joust (USA) (Proto)
# 7 - Miner 2049er Starring Bounty Bob (USA, Europe) (v1.1)
# 8 - Mr. Do! (USA, Europe)
# 9 - Zaxxon (USA, Europe)
#10 - A.E. (USA) (Proto)
#11 - Boulder Dash (USA)
#12 - Choplifter! (USA)
#13 - Frogger II - ThreeeDeep! (USA)
#14 - H.E.R.O. - Helicopter Emergency Rescue Operation (USA)
#15 - Pitfall II - Lost Caverns (USA)
```

##### Flasher votre EPROM/EEPROM

Nous vous recommandons d'utiliser un programmateur d'EPROM de type "TS866 II Plus". Mais n'importe quel programmeur d'EPROM capable de programmer les séries 27 fonctionnera. Cependant, les examples de cette documentation font référence au TL866 II Plus.

Si vous n'avez pas de programmateur d'EPROM et que vous souhaitez en acquérir un, vous pouvez trouver les modèles [TL866 II Plus ici](https://fr.aliexpress.com/w/wholesale-TL866-II-plus.html) à des prix corrects (la version blanche/grise autour de 50€ est amplement suffisante). 

Lancez le logiciel du programmateur, puis cliquer sur le bouton dans le cadre "Select IC". Cherchez "27C040" dans "Search device", sélectionnez la marque de l'EEPROM (Winbond ici), puis validez.

![modderbrothers](./assets/eprom-select.png)

Ensuite, sur l'interface principale, cliquez sur le bouton "LOAD" en haut à gauche pour charger votre fichier à programmer (celui que vous avez sauvé avec l'application "Rom Merger"). Sélectionnez-le avec le bouton "BROWSE" puis validez sur "OK".

![modderbrothers](./assets/load-file.png)

Une fois chargé, la visualisation binaire de votre fichier apparait à gauche :

![modderbrothers](./assets/file-loaded.png)

Il ne reste plus qu'à programmer votre EEPROM !

Cliquez sur le bouton "PROG" and haut de l'interface principale, puis lancer le flash en cliquant sur le bouton "Program".

Si vous obtenez une erreur à la vérification de l'ID de l'EEPROM, vérifiez que la marque et le modèle d'EEPROM sélectionnés soit le bon ! Ensuite, relancez la programmation.
 
Si l'erreur continue d'apparaitre ou si vous ne trouvez pas le bon modèle de la bonne marque, décochez la case à cocher "Check ID" en bas à gauche de l'interface. Puis relancez la programmation. 

![modderbrothers](./assets/flash-in-progress.png)

Une fois la programmation terminée, le logiciel va faire une passe de vérification et si tout s'est bien passé, vous devriez obtenir :

![modderbrothers](./assets/flash-ok.png)

C'est terminé ! Il ne vous reste plus qu'à remonter votre cartouche.

#### Remontage de la cartouche

Pour remonter la cartouche, procédez en sens inverse du démontage :
- Insérez l'EEPROM délicatement dans son support. Vérifiez que toutes les pates soient bien alignées et positionnées dans les trous du support avant d'appuyer ! Sans ça vous risquez de tordre des broches. 
- Replacez le PCB dans le bon sens dans la cartouche
- Revissez les vis

Il ne vous restera plus qu'à tester vos nouveaux jeux !