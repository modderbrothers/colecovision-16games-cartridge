![modderbrothers](../assets/product.png)

# Manual - Colecovision - 16 Games Cartridge

[Version Française ici](./LISEZMOI.md)

## Tools required

Required for Cartridge usage:
- None
 
For reprogramming the EEPROM or changing the EPROM/EEPROM:
- Screwdriver
- Toothless knife
- EPROM programmer capable of programming the 27 series (_**TL866 II Plus**_ or similar)

## Using the cartridge

Simply insert the cartridge into your Colecovision, just like any other game cartridge, and start it up!
The game corresponding to the position of the small rotary selector will start.

To change games :
- Turn off your console
- Move the rotary button to the desired position
- Turn your console back on

Moving the rotary button while the console is on will not damage your console or the cartridge, but will cause the current game to crash.

![modderbrothers](./assets/cartridge-in-action.png)

## Reprogramming and/or changing the EPROM/EEPROM

#### General precautions

- All manipulations must be carried out without power.
- You are responsible for your tools and their handling. If you do not have the required skills and/or experience, we advise you to have these manipulations carried out by a qualified person.

#### Precautions to be taken before touching the boards and electronic components

Some components are very sensitive to static electricity. Be sure to discharge yourself before opening your console cartridge:
- Place yourself in a tiled room, and avoid carpeting!
- Avoid wool sweaters or other clothing that charges easily!
- Touch the metal casing of an electrical appliance to discharge to the ground.

#### Opening the cartridge

Simply unscrew the screws of the cartridge until the 2 parts of the shell separate

![modderbrothers](./assets/cartridge-open.png)

#### Extracting the EPROM

It is now necessary to extract the EEPROM from its socket:
To do this, slide ***very gently*** the blade of the knife between the EEPROM and its socket.
Then turn the knife slightly clockwise and counter-clockwise so that the blade lifts the chip by one millimeter on each side of the tabs.
Repeat this process alternately on each side of the socket until the chip pops out on its own.
If possible, avoid touching the metal tabs.

Be careful not to tip the knife on the motherboard, it could damage the tracks underneath!

![modderbrothers](./assets/cartridge-eprom-extracted.png)

##### Compatible EPROM/EEPROM

Compatible EPROMs are all 27C040 EPROMs (512Kb - 4MBits).
The compatible EEPROMs are all 27040 EEPROMs (W27E040, 27E040, ...)

All the EPROM/EEPROM compatible pin to pin with the 27C040 should work also.
To avoid any problems related to the speed of these chips, take the fastest versions: those where the number behind the '-' is the lowest.

![modderbrothers](./assets/compatible-eprom-eeprom.png)

#### Reprogramming

To change your games on the cartridge, you have two solutions:
- Erase the current EPROM and reprogram it
- Program a new EPROM and replace the original one (which allows you to keep it and put it back later)

##### Delete the EPROM/EEPROM

You can skip this step if:
- Your cartridge contains an EEPROM (Electrically Erasable Programmable Read-Only Memory), without a small visible pane of glass, which is the case on ModderBrothers cartridges by default.
- If you have opted for an EPROM/EEPROM replacement, you can skip this step

In the last case, you will have to make sure that your EPROM is totally erased. Use a small Ultra-Violet device that can be found easily, and for cheap here :  
https://www.amazon.fr/s?k=eprom+uv+eraser
or a little cheaper here :
https://fr.aliexpress.com/w/wholesale-eprom-uv-eraser.html

##### Build your flash file

Get the latest version of our [Rom Merger](https://gitlab.com/modderbrothers/rom-merger-application/-/releases) (under Windows only for now)

Download the application directly, then unzip it in the directory of your choice, before launching it.

![modderbrothers](./assets/rom-merger.png)

Its use is extremely simple: start by selecting the right cartridge "Colecovision 16 games cartridge".

Then add your roms files in ".bin" or ".a26" format. You can reorder them as you wish, even after you have added them. When the file selector opens, use SHIFT or CTRL to select several files at once.
The selected files must, of course, respect the 32kb limit per rom.

Once your files are selected and ordered, simply click on "Save". The application will build a flat file, _ready to program_, containing all your roms.

By default, the checkbox "Autofill empty slot" is checked. This means that if you have not filled all 16 available slots, the software will fill the gaps by taking the first roms.

Once saved, the application displays a report of the operations.

![modderbrothers](./assets/rom-merger-saved.png)

Next to the ".bin" file that will have been created, you will also find a ".txt" file with the same name, which will contain the list of rom per slot. Example:

```
# 0 - Dukes of Hazzard, The (USA)
# 1 - Spy Hunter (USA)
# 2 - Super DK! (USA) (Proto)
# 3 - Buck Rogers - Planet of Zoom (USA, Europe)
# 4 - Defender (USA) (BUG Fixed)
# 5 - Donkey Kong (USA)
# 6 - Joust (USA) (Proto)
# 7 - Miner 2049er Starring Bounty Bob (USA, Europe) (v1.1)
# 8 - Mr. Do! (USA, Europe)
# 9 - Zaxxon (USA, Europe)
#10 - A.E. (USA) (Proto)
#11 - Boulder Dash (USA)
#12 - Choplifter! (USA)
#13 - Frogger II - ThreeeDeep! (USA)
#14 - H.E.R.O. - Helicopter Emergency Rescue Operation (USA)
#15 - Pitfall II - Lost Caverns (USA)
```

##### Flashing your EPROM/EEPROM

We recommend that you use a "TS866 II Plus" EPROM programmer. But any EPROM programmer capable of programming the 27 series will work. However, the examples in this documentation refer to the TL866 II Plus.

If you don't have an EPROM programmer and want to buy one, you can find the [TL866 II Plus here](https://fr.aliexpress.com/w/wholesale-TL866-II-plus.html) models at decent prices (the white/gray version around 50€ is more than enough).

Launch the programmer software, then click on the button in the "Select IC" box. Search for "27C040" in "Search device", select the EEPROM brand (Winbond here), then validate.

![modderbrothers](./assets/eprom-select.png)

Then, on the main interface, click on the "LOAD" button at the top left to load your file to be programmed (the one you saved with the "Rom Merger" application). Select it with the "BROWSE" button then validate with "OK".

![modderbrothers](./assets/load-file.png)

Once loaded, the binary visualization of your file appears on the left:

![modderbrothers](./assets/file-loaded.png)

All that's left is to program your EEPROM!

Click on the "PROG" button at the top of the main interface, then start the flash by clicking on the "Program" button.

If you get an error when checking the EEPROM ID, make sure that the brand and model of EEPROM selected are the right ones! Then restart the programming.

If it still fails or if you do not find the right model of the right brand, uncheck the "Check ID" checkbox at the bottom left of the interface. Then restart the programming.

![modderbrothers](./assets/flash-in-progress.png)

Once the programming is finished, the software will make a verification pass and if everything went well, you should get:

![modderbrothers](./assets/flash-ok.png)

That's it! All you have to do is reassemble your cartridge.

#### Reassembling the cartridge

To reassemble the cartridge, proceed in the reverse order of disassembly:
- Insert the EEPROM gently into its socket. Check that all the pins are well aligned and positioned in the holes of the socket before pressing! Otherwise, you may bend some pins.
- Replace the PCB in the right place in the cartridge
- Screw the screws back in

Now all you have to do is test your new games!